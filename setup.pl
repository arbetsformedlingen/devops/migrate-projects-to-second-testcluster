#!/usr/bin/env perl
use strict;
use warnings;
use CPAN;
use FindBin qw($Bin);
use local::lib "$Bin/lib";

CPAN::install("GitLab::API::v4");
CPAN::install("File::Slurp");
CPAN::install("YAML::Tiny");
CPAN::install("URI::Encode");
