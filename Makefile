

check-webhooks:
	src/webhookotron.pl --groupid 7094694

check-infra:
	src/infratron.pl --groupid 7094694  --argocdinfraid 33413433 --use-existing-cache 0

check-infra-test:
	src/infratron.pl --groupid 15917992 --argocdinfraid 40538521 --use-existing-cache 0
