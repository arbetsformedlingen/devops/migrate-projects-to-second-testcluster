#!/usr/bin/env perl
use strict;
use warnings;
use Getopt::Long;
use Storable;
use FindBin qw($Bin);
use List::Util qw(reduce);
use Data::Dumper;
local $Data::Dumper::Terse = 1;

use local::lib "$Bin/../lib";
use GitLab::API::v4;
use File::Slurp;

use lib "$Bin/../secrets";
use WebhookotronConfig;

use lib "$Bin";
use Migrate;



chomp(my $gitlabsecret = read_file("$Bin/../secrets/gitlab.txt")) || die($!);

my $api = GitLab::API::v4->new(
    url           => $WebhookotronConfig::gitlab_v4_api_url,
    private_token => $gitlabsecret,
    );


#my $topgroup = "7094694"; #production!
my $topgroup = "59285745"; # safe testing!

my $do_it    = 0; # actually perform changes in Gitlab - otherwise just dry run

# stats
my $hooks_total = 0;
my $use_existing_cache=1;

GetOptions ("groupid=s" => \$topgroup,
            "do-it"  => \$do_it,
            "use-existing-cache=i" => \$use_existing_cache,
    )
    or die("Error in command line arguments\ngroupid=s\ndo-it");

print "gitlab group to iterate:${topgroup}\n";
print "do it: ${do_it}\n";
print "use cache: ${use_existing_cache}\n";


###############################################################################
#### SUBROUTINES



sub process_webhooks {
    my $proj = shift;

    my $hooks = $api->project_hooks($proj->{'id'});
    if(@{$hooks}) {
	map {
	    my $hook = $_;
	    my $url = $hook->{'url'};

	    # ignore noise flood (for now: snyk-urls)
	    if($url !~ /$WebhookotronConfig::webhook_ignore_url_regex/) {
	    	print STDERR "\n Hook found(repo,projid,url): https://gitlab.com/".$proj->{'path_with_namespace'},"/-/hooks [".$proj->{'id'}."] ",$url,"\n";
                $hooks_total++;
		# simple normalisation
		$url =~ s|/$||;
		$url =~ s|^http://|https://|;

		if(defined($WebhookotronConfig::webhook_actions{$url})) {
		    my $op = $WebhookotronConfig::webhook_actions{$url}->{'op'};
		    if($op eq 'delete') {
			delete_hook($proj->{'id'}, $hook->{'id'});
		    } elsif($op eq 'replace') {
			delete_hook($proj->{'id'}, $hook->{'id'});
			create_hook($proj->{'id'}, $WebhookotronConfig::webhook_actions{$url}->{'url'}, $WebhookotronConfig::webhook_actions{$url}->{'token'});
		    } elsif($op eq 'ignore') {
			# nop
		    } else {
			warn "\tUnknown op: $op";
		    }
		} else {
		    warn "\tDefine action for ".$url;
		}
	    }
	} @{$hooks};
    }
}



sub create_hook {
    my $proj_id = shift;
    my $url     = shift;
    my $token = shift;
    my $params  = shift // { 'enable_ssl_verification' => 0,
				 'merge_requests_events' => 1,
				 'push_events' => 1,
				 'tag_push_events' => 1 };
    my $paramsstr = Dumper($params);

    $params->{'url'} = $url;
    $params->{'token'} = $token;
    $paramsstr =~ s/\n//g;
    $paramsstr =~ s/\s+/ /g;

    if($do_it) {
	warn "\t\$api->create_project_hook($proj_id, $paramsstr);";
        if (Migrate::query_user("create hook?")) {
            $api->create_project_hook($proj_id, $params);
	}
    } else {
	warn "\tDRY RUN: \$api->create_project_hook($proj_id, $paramsstr);";
    }
}



sub delete_hook {
    my $proj_id = shift;
    my $hook_id = shift;

    if($do_it) {
	warn "\t\$api->delete_project_hook($proj_id, $hook_id);";
        if(Migrate::query_user("delete hook?")) {
	    $api->delete_project_hook($proj_id, $hook_id);
	}
    } else {
	warn "\tDRY RUN: \$api->delete_project_hook($proj_id, $hook_id);";
    }
}



###############################################################################
#### MAIN
my @projs;

if ($use_existing_cache) {
    my $cached_group = read_file('cache.groupid')    || die("cannot find cache.groupid");
    chomp($cached_group);

    die("cache mismatch, exiting. Cached group: $cached_group, current group: $topgroup")
	unless($topgroup eq "$cached_group");

    (@projs = @{retrieve('cache.store')})              || die("no cache.store, create it please!");

} else {
    write_file('cache.groupid', $topgroup);

    say STDERR "Reading descendant projects of $topgroup...";
    @projs = Migrate::get_descendant_projects($api, $gitlabsecret, "$topgroup");
    store(\@projs, 'cache.store');

    say STDERR "Cached stored.";
}

map { process_webhooks($_) } @projs;

print "\n\n";
print "total hooks processed: ${hooks_total}\n"
