#!/usr/bin/env perl
use strict;
use warnings;
use feature qw(say);
use MIME::Base64;
use Getopt::Long;
use Storable;
use FindBin qw($Bin);
use Data::Dumper;
local $Data::Dumper::Terse = 1;

use local::lib "$Bin/../lib";
use GitLab::API::v4;
use File::Slurp;
use YAML::Tiny;

use lib "$Bin";
use Migrate;

use lib "$Bin/../secrets";
use WebhookotronConfig;



chomp(my $gitlabsecret = read_file("$Bin/../secrets/gitlab.txt")) || die($!);

my $api = GitLab::API::v4->new(
    url           => $WebhookotronConfig::gitlab_v4_api_url,
    private_token => $gitlabsecret,
    );


#my $argocd_infra_repo_id = 33413433; # prod
my $argocd_infra_repo_id = 40538521; # testing https://gitlab.com/arbetsformedlingen/devops/per-test/fault-test-argocd-infra

my $ns_conf_infra_repo_id = 38135317;

#my $topgroup = "7094694"; #production!
my $topgroup = "59285745"; # safe testing!

my $do_it    = 0; # actually perform changes in Gitlab - otherwise just dry run

my $loglevel = 'warn';
my $logtype;

# stats
my $projs_total = 0;
my $use_existing_cache=1;

GetOptions ("groupid=s" => \$topgroup,
	    "argocdinfraid=s" => \$argocd_infra_repo_id,
            "do-it"  => \$do_it,
            "use-existing-cache=i" => \$use_existing_cache,
	    "loglevel=s" => \$loglevel,
	    "logtype=s" => \$logtype,
    )
    or die("Error in command line arguments\ngroupid=s\ndo-it");

print "gitlab group to iterate:${topgroup}\n";
print "do it: ${do_it}\n";
print "use cache: ${use_existing_cache}\n";


###############################################################################
#### SUBROUTINES


sub lint_project_argocd {
    my $validationref          = shift;
    my $argocd_proj            = shift;
    my $argo_files             = shift;
    my $projkustcontentyamlobj = shift;
    my $proj                   = shift;
    my $overlay                = shift;
    my $projs                  = shift;

    my $warnings = 0;
    my $Pbasename = $proj->{'path'}; # the basename of the gitlab project slug
    $Pbasename =~ s/-infra$//;

    my @found_argo = grep { $_->{'path'} eq "kustomize/clusters/testing/".$Pbasename."-".$overlay.".yaml" } @{$argo_files};
    unless(@found_argo) {
	push(@{$validationref}, { 'severity' => 'warn', 'type' => '2', 'proj' => $proj->{'path_with_namespace'},
				      'msg' => "expected argocd-infra to contain file kustomize/clusters/testing/".$Pbasename."-".$overlay.".yaml",
				      'advice' => 'Open '.$argocd_proj->{'web_url'}."/-/tree/".$argocd_proj->{'default_branch'}."/kustomize/clusters/testing and create ".$Pbasename."-".$overlay.".yaml" });
    }

    my @test1 = grep { $_->{'path'} eq $Pbasename } @{$projs};
    unless(@test1) {
	my $app_repo = $proj->{'web_url'};
	$app_repo =~ s/-infra$//;
	push(@{$validationref}, { 'severity' => 'warn', 'type' => '3', 'proj' => $proj->{'path_with_namespace'},
				      'msg' => "expected to find a matching project '$Pbasename' to ".$proj->{'path'}." on Gitlab",
				      'advice' => "Create application repo $app_repo or remove infra repo ".$proj->{'web_url'} });
    }

    if(@found_argo) {
	unless(defined($found_argo[0]->{'content'}->[0]->{'spec'}->{'source'}->{'repoURL'})
	       && ( $found_argo[0]->{'content'}->[0]->{'spec'}->{'source'}->{'repoURL'} eq $proj->{'ssh_url_to_repo'}
		    || $found_argo[0]->{'content'}->[0]->{'spec'}->{'source'}->{'repoURL'} eq $proj->{'http_url_to_repo'})) {
	    push(@{$validationref}, { 'severity' => 'warn', 'type' => '4', 'proj' => $proj->{'path_with_namespace'},
					  'msg' => "repo links do not match (argocd, ".$Pbasename."-infra): ".$found_argo[0]->{'content'}->[0]->{'spec'}->{'source'}->{'repoURL'}.", ".$proj->{'http_url_to_repo'},
					  'advice' => 'Edit spec->source->repoURL in '.$argocd_proj->{'web_url'}."/-/tree/".$argocd_proj->{'default_branch'}."/kustomize/clusters/testing/".$Pbasename."-".$overlay.".yaml and enter value ".$proj->{'http_url_to_repo'} });
	}

	unless(defined($found_argo[0]->{'content'}->[0]->{'spec'}->{'source'}->{'path'})
	       && $found_argo[0]->{'content'}->[0]->{'spec'}->{'source'}->{'path'} eq "kustomize/overlays/".$overlay) {
	    push(@{$validationref}, { 'severity' => 'warn', 'type' => '5', 'proj' => $proj->{'path_with_namespace'},
					  'msg' => "repo paths do not match (argocd, ".$Pbasename."-infra): ".$found_argo[0]->{'content'}->[0]->{'spec'}->{'source'}->{'path'}.", kustomize/overlays/".$overlay,
					  'advice' => 'Edit spec->source->path in '.$argocd_proj->{'web_url'}."/-/tree/".$argocd_proj->{'default_branch'}."/kustomize/clusters/testing/".$Pbasename."-".$overlay.".yaml and enter value $overlay" });
	}

	unless(defined($found_argo[0]->{'content'}->[0]->{'metadata'}->{'namespace'})
	       && $found_argo[0]->{'content'}->[0]->{'metadata'}->{'namespace'} eq "openshift-gitops") {
	    push(@{$validationref}, { 'severity' => 'warn', 'type' => '6', 'proj' => $proj->{'path_with_namespace'},
					  'msg' => "expected argocd content->metadata->namespace to contain openshift-gitops ("
					  .$found_argo[0]->{'content'}->[0]->{'metadata'}->{'namespace'}.")",

					  'advice' => 'Edit metadata->namespace in '.$argocd_proj->{'web_url'}."/-/tree/".$argocd_proj->{'default_branch'}."/kustomize/clusters/testing/".$Pbasename."-".$overlay.".yaml and enter value openshift-gitops" });
	}

	unless(defined($found_argo[0]->{'content'}->[0]->{'metadata'}->{'name'})
	       && $found_argo[0]->{'content'}->[0]->{'metadata'}->{'name'} eq $Pbasename."-".$overlay) {
	    push(@{$validationref}, { 'severity' => 'warn', 'type' => '7', 'proj' => $proj->{'path_with_namespace'},
					  'msg' => "expected argocd content->metadata->name to contain ".$Pbasename."-".$overlay." ("
					  .$found_argo[0]->{'content'}->[0]->{'metadata'}->{'name'}.")",
					  'advice' => 'Open '.$argocd_proj->{'web_url'}.'/-/tree/'.$proj->{'default_branch'}.'/'.$found_argo[0]->{'path'}.' and set metadata->name to '.$Pbasename."-".$overlay });
	}

	unless(defined($found_argo[0]->{'content'}->[0]->{'spec'}->{'destination'}->{'namespace'})
	       && $found_argo[0]->{'content'}->[0]->{'spec'}->{'destination'}->{'namespace'} eq $Pbasename."-".$overlay) {
	    push(@{$validationref}, { 'severity' => 'warn', 'type' => '8', 'proj' => $proj->{'path_with_namespace'},
					  'msg' => "expected argocd content->spec->destination->namespace to contain ".$Pbasename."-".$overlay." ("
					  .$found_argo[0]->{'content'}->[0]->{'spec'}->{'destination'}->{'namespace'}.")",
					  'advice' =>  'Open '.$argocd_proj->{'web_url'}.'/-/tree/'.$proj->{'default_branch'}.'/'.$found_argo[0]->{'path'}.' and set spec->destination->namespace to '.$Pbasename."-".$overlay });
	}
    }

    if($projkustcontentyamlobj->{'namespace'} ne $Pbasename."-".$overlay) {
	push(@{$validationref}, { 'severity' => 'warn', 'type' => '9', 'proj' => $proj->{'path_with_namespace'},
				      'msg' => "expected ".$Pbasename."-infra/kustomize/overlays/$overlay/kustomization.yaml to use namespace ".$Pbasename."-".$overlay." (".$projkustcontentyamlobj->{'namespace'}.")",
				      'advice' => 'Open '.$proj->{'web_url'}."/-/tree/".$proj->{'default_branch'}."/kustomize/overlays/$overlay/kustomization.yaml and set namespace to $Pbasename"."-".$overlay });
    }
}



sub lint_project {
    my $validationref = shift;
    my $argocd_proj   = shift;
    my $argo_files    = shift;
    my $ns_conf_files = shift;
    my $proj          = shift;
    my $overlay       = shift;
    my $projs         = shift;

    #say STDERR "\n\nlinting ".$proj->{'path_with_namespace'};

    # Check if overlay exists
    if(my $projkustfobj = $api->file($proj->{'id'}, "kustomize/overlays/$overlay/kustomization.yaml", { 'ref' => 'HEAD' })) {
	my $projkustcontent = decode_base64($projkustfobj->{'content'});
	my $projkustcontentyaml = YAML::Tiny->read_string($projkustcontent);

	# Go through all yaml objects in the file and find one with a namespace
	# (if there is a namespace set, it's probably the right object in the yaml file (in case there are >1))
	my @cands = grep { defined($_->{'namespace'}) } @{$projkustcontentyaml};
	if(@cands != 1) {
	    push(@{$validationref}, { 'severity' => 'warn', 'type' => '0', 'proj' => $proj->{'path_with_namespace'},
					  'msg' => "expected only one object with namespace set in kustomize/overlays/$overlay/kustomization.yaml",
					  'advice' => 'Open '.$proj->{'web_url'}."/-/tree/".$proj->{'default_branch'}."/kustomize/overlays/$overlay/kustomization.yaml and make sure there is one and only one welformed yaml entry" });
	    @cands = ($cands[0]); # continue with the first match only
	}

	if(@cands) {
	    # Run the argocd checks
	    lint_project_argocd($validationref, $argocd_proj, $argo_files, $cands[0], $proj, $overlay, $projs);
	}

    } else {
	push(@{$validationref}, { 'severity' => 'warn', 'type' => '1', 'proj' => $proj->{'path_with_namespace'},
				      'msg' => "no $overlay layer in ".$proj->{'path_with_namespace'},
				      'advice' => 'Open '.$proj->{'web_url'}."/-/tree/".$proj->{'default_branch'}."/kustomize and create overlays/$overlay/kustomization.yaml" });
    }
}



###############################################################################
#### MAIN
my @projs;
my $ns_conf_files;
my $argo_files;
my @validation;


if ($use_existing_cache) {
    my $cached_group = read_file('cache.groupid')    || die("cannot find cache.groupid");
    chomp($cached_group);

    die("cache mismatch, exiting. Cached group: $cached_group, current group: $topgroup")
	unless($topgroup eq "$cached_group");

    (@projs = @{retrieve('cache.store')})            || die("no cache.store, create it please!");
#    $ns_conf_files = retrieve('cache.ns_conf.store') || die("no cache.ns_conf.store, create it please");
    $argo_files = retrieve('cache.argo.store')       || die("no cache.argo.store, create it please");

    if ($use_existing_cache == 2) {
	(@validation = @{retrieve('cache.validation.store')})            || die("no cache.validation.store, create it please!");
    }

} else {
    write_file('cache.groupid', $topgroup);

    say STDERR "Reading descendant projects of $topgroup...";
    @projs = grep { $_->{'path_with_namespace'} !~ /\/attic\// }
    Migrate::get_descendant_projects($api, $gitlabsecret, "$topgroup");
    store(\@projs, 'cache.store');

#    say STDERR "Reading ns-conf...";
#    $ns_conf_files = Migrate::read_directory($api, $ns_conf_infra_repo_id, "kustomize/overlays");
#    store($ns_conf_files, 'cache.ns_conf.store');

    say STDERR "Reading argocd...";
    $argo_files = Migrate::read_directory($api, $argocd_infra_repo_id, "kustomize/clusters");
    store($argo_files, 'cache.argo.store');

    say STDERR "Cached stored.";
}


my $argocd_proj = Migrate::get_project($api, $argocd_infra_repo_id);


if ($use_existing_cache < 2) {
    map { lint_project(\@validation, $argocd_proj, $argo_files, $ns_conf_files, $_, "develop", \@projs ) } grep { $_->{'path'} =~ /-infra$/ } @projs;
    #map { lint_project(\@validation, $argocd_proj, $argo_files, $ns_conf_files, $_, "test", \@projs ) } grep { $_->{'path'} =~ /-infra$/ } @projs;
    store(\@validation, 'cache.validation.store');
}



#### Per project validation
@validation = grep { $_->{'severity'} eq $loglevel } @validation;

if(defined($logtype)) {
    @validation = grep { $_->{'type'} eq $logtype } @validation;
}

map {
    printf "[severity %s][type %s][proj %s]\n\tMsg:\t\t%s\n\n", $_->{'severity'}, $_->{'type'}, $_->{'proj'}, $_->{'msg'}.(defined($_->{'advice'}) ? "\n\tAdvice:\t".$_->{'advice'} : '');
} @validation;
say "\nPrinted ".@validation." validation notices.";



#### test vs. testing
my @in_test    = grep { $_->{'path'} =~ m/kustomize\/clusters\/test\// } @{$argo_files};
my %in_testing;
map { $in_testing{$_->{'name'}} = $_ } grep { $_->{'path'} =~ m/kustomize\/clusters\/testing\// } @{$argo_files};
my @missing_in_testing = grep { ! defined($in_testing{$_->{'name'}}) } @in_test;
if(@missing_in_testing) {
    say "\n\nApplications in argocd-infra cluster/test missing in cluster/testing:";
    map {
	say "\t".$_->{'name'};
    } sort { $a->{'name'} cmp $b->{'name'} } @missing_in_testing;
}
