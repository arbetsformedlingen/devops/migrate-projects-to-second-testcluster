package Migrate;
use List::Util qw(reduce);
use MIME::Base64;

use FindBin qw($Bin);
use local::lib "$Bin/../lib";
use GitLab::API::v4;
use URI::Encode qw(uri_encode);
use JSON;



sub read_directory {
    my $api     = shift;
    my $proj_id = shift;
    my $path    = shift;

    my @result = ();

    if(my $tree = $api->paginator('tree', $proj_id, { 'recursive' => 1, 'path' => $path })->all()) {
	foreach my $file (grep { $_->{'path'} =~ /\.yaml$/ } @{$tree}) {
	    if(my $fileobj = $api->file($proj_id, $file->{'path'}, { 'ref' => 'HEAD' })) {
		my $content = decode_base64($fileobj->{'content'});
		my $yaml = YAML::Tiny->read_string($content);
		$file->{'content'} = $yaml;
		push(@result, $file);
	    }
	}
    }

    return \@result;
}




sub get_descendant_groups {
    my $gitlabsecret = shift;
    my $group_id     = shift;

    my $page = 1;
    my @results = ($group_id);

    while(my $response = `curl --silent --header "PRIVATE-TOKEN: $gitlabsecret" "$WebhookotronConfig::gitlab_v4_api_url/groups/$group_id/descendant_groups?per_page=10&page=$page" | jq -r '.[] | .id'`) {
	@results = (@results, split(/\n/, $response));
	$page++;
    }
    return @results;
}



sub get_descendant_projects {
    my $api          = shift;
    my $gitlabsecret = shift;
    my $group_id     = shift;

    return @{reduce(sub {[
                        @{$a},
                        @{$api->paginator('group_projects', $b, { 'archived' => 0 })->all()} ]},
                    [], get_descendant_groups($gitlabsecret, $group_id))};
}



sub get_project {
    my $api     = shift;
    my $proj_id = shift;

    return $api->project($proj_id, {});
}



sub query_user {
    my $prompt = shift;

    print "$prompt Y|N|Q:";
    while (1){
        chomp(my $input = <STDIN>);

        if ($input =~ /^[Y]$/i) {      # Match Yy or blank
            return 1;
        } elsif ($input =~ /^[N]$/i) {  # Match Nn
            return 0;
        } elsif ($input =~ /^[Q]$/i) {  # Match Qq
            die "die";
        } else {
        }
    }

}



1;
